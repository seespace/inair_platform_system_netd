/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>

#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <netinet/in.h>
#include <arpa/inet.h>

#include <linux/wireless.h>

#include <openssl/evp.h>
#include <openssl/sha.h>

#define LOG_TAG "SoftapController"
#include <cutils/log.h>
#include <netutils/ifc.h>
#include <private/android_filesystem_config.h>
#include "wifi.h"
#include "ResponseCode.h"

#include "SoftapController.h"
#include <cutils/properties.h>  /* modifed by clei for set wifi ap channel */
#ifdef BOARD_WIFI_MTK
#undef HAVE_HOSTAPD
static const char RALINK_AP_DAT[] = "/data/misc/wifi/RT2870AP.dat";
#endif

static const char HOSTAPD_CONF_FILE[]    = "/data/misc/wifi/hostapd.conf";
static const char HOSTAPD_BIN_FILE[]    = "/system/bin/hostapd";

SoftapController::SoftapController()
    : mPid(0) {}

SoftapController::~SoftapController() {
}

int SoftapController::startSoftap() {
    pid_t pid = 1;
	int ret = 0;

    if (mPid) {
        ALOGE("SoftAP is already running");
        return ResponseCode::SoftapStatusResult;
    }
#ifdef HAVE_HOSTAPD
    if ((pid = fork()) < 0) {
        ALOGE("fork failed (%s)", strerror(errno));
        return ResponseCode::ServiceStartFailed;
    }
#endif
    if (!pid) {
#ifdef HAVE_HOSTAPD
        ensure_entropy_file_exists();
        if (execl(HOSTAPD_BIN_FILE, HOSTAPD_BIN_FILE,
                  "-e", WIFI_ENTROPY_FILE,
                  HOSTAPD_CONF_FILE, (char *) NULL)) {
            ALOGE("execl failed (%s)", strerror(errno));
        }
#endif
        ALOGE("SoftAP failed to start");
        return ResponseCode::ServiceStartFailed;
    } else {
        system("netcfg wlan0 up");
        ret = 0;
        //ret = setCommand(mIface, "AP_BSS_START");
        if (ret) {
            ALOGE("Softap startap - failed: %d", ret);
        }
        else {
            mPid = pid;
            ALOGD("SoftAP started successfully");
            usleep(AP_BSS_START_DELAY);
        }
    }
    return ResponseCode::SoftapStatusResult;
}

int SoftapController::stopSoftap() {

    if (mPid == 0) {
        ALOGE("SoftAP is not running");
        return ResponseCode::SoftapStatusResult;
    }

#ifdef HAVE_HOSTAPD
    ALOGD("Stopping Softap service");
    kill(mPid, SIGTERM);
    waitpid(mPid, NULL, 0);
#endif
    if (mSock < 0) {
        ALOGE("Softap stopap - failed to open socket");
        return -1;
    }
    system("netcfg wlan0 down");

    mPid = 0;
    ALOGD("SoftAP stopped successfully");
    usleep(AP_BSS_STOP_DELAY);
    return ResponseCode::SoftapStatusResult;
}

bool SoftapController::isSoftapStarted() {
    return (mPid != 0);
}

/*
 * Arguments:
 *  argv[2] - wlan interface
 *  argv[3] - SSID
 *  argv[4] - Broadcast/Hidden
 *  argv[5] - Channel
 *  argv[6] - Security
 *  argv[7] - Key
 */
int SoftapController::setSoftap(int argc, char *argv[]) {
    char psk_str[2*SHA256_DIGEST_LENGTH+1];
    int ret = ResponseCode::SoftapStatusResult;
    int i = 0;
    int fd;
    int hidden = 0;
    int channel = AP_CHANNEL_DEFAULT;
    char *wbuf = NULL;
    char *fbuf = NULL;

    if (argc < 5) {
        ALOGE("Softap set is missing arguments. Please use:");
        ALOGE("softap <wlan iface> <SSID> <hidden/broadcast> <channel> <wpa2?-psk|open> <passphrase>");
        return ResponseCode::CommandSyntaxError;
    }

#ifdef HAVE_HOSTAPD
    if (!strcasecmp(argv[4], "hidden"))
        hidden = 1;

    if (argc >= 5) {
        channel = atoi(argv[5]);
        if (channel <= 0)
            channel = AP_CHANNEL_DEFAULT;
    }
    /* modifed by clei for set wifi ap channel */
    char *wifi_type = NULL;
    char chn[PROPERTY_VALUE_MAX] = {0};
    property_get("persist.sys.ap.channel", chn, "6");
    channel = atoi(chn);
    if(channel <= 14)
    	asprintf(&wifi_type, "%s", "b");
    else
    	asprintf(&wifi_type, "%s", "a");
   
    asprintf(&wbuf, "interface=%s\ndriver=nl80211\nctrl_interface="
            "/data/misc/wifi/hostapd\nssid=%s\nchannel=%d\nieee80211n=1\n"
            "hw_mode=%s\nignore_broadcast_ssid=%d\n",
            argv[2], argv[3], channel, wifi_type,hidden);

    if (argc > 7) {
        if (!strcmp(argv[6], "wpa-psk")) {
            generatePsk(argv[3], argv[7], psk_str);
            asprintf(&fbuf, "%swpa=1\nwpa_pairwise=TKIP CCMP\nwpa_psk=%s\n", wbuf, psk_str);
        } else if (!strcmp(argv[6], "wpa2-psk")) {
            generatePsk(argv[3], argv[7], psk_str);
            asprintf(&fbuf, "%swpa=2\nrsn_pairwise=CCMP\nwpa_psk=%s\n", wbuf, psk_str);
        } else if (!strcmp(argv[6], "open")) {
            asprintf(&fbuf, "%s", wbuf);
        }
    } else if (argc > 6) {
        if (!strcmp(argv[6], "open")) {
            asprintf(&fbuf, "%s", wbuf);
        }
    } else {
        asprintf(&fbuf, "%s", wbuf);
    }

    fd = open(HOSTAPD_CONF_FILE, O_CREAT | O_TRUNC | O_WRONLY | O_NOFOLLOW, 0660);
    if (fd < 0) {
        ALOGE("Cannot update \"%s\": %s", HOSTAPD_CONF_FILE, strerror(errno));
        free(wbuf);
        free(fbuf);
        return ResponseCode::OperationFailed;
    }
    if (write(fd, fbuf, strlen(fbuf)) < 0) {
        ALOGE("Cannot write to \"%s\": %s", HOSTAPD_CONF_FILE, strerror(errno));
        ret = ResponseCode::OperationFailed;
    }
    free(wbuf);
    free(fbuf);

    /* Note: apparently open can fail to set permissions correctly at times */
    if (fchmod(fd, 0660) < 0) {
        ALOGE("Error changing permissions of %s to 0660: %s",
                HOSTAPD_CONF_FILE, strerror(errno));
        close(fd);
        unlink(HOSTAPD_CONF_FILE);
        return ResponseCode::OperationFailed;
    }

    if (fchown(fd, AID_SYSTEM, AID_WIFI) < 0) {
        ALOGE("Error changing group ownership of %s to %d: %s",
                HOSTAPD_CONF_FILE, AID_WIFI, strerror(errno));
        close(fd);
        unlink(HOSTAPD_CONF_FILE);
        return ResponseCode::OperationFailed;
    }
#else
    /* Create command line */
    char *wbuf = NULL;
    char *fbuf = NULL;
    
	
	if (argc > 3) {
        ssid = argv[3];
    } else {
        ssid = (char *)"AndroidAP";
    }
    
	ALOGE("Softap set SSID = %s, SEC = %s, KEY = %s, Channel = %s", ssid, argv[3], argv[4], argv[5]);
	
    asprintf(&wbuf, "Default\nBssidNum=1\nWirelessMode=9\nCountryRegion=5\n"
            "Channel=6\nSSID=%s\nBeaconPeriod=100\nTxPower=100\nRTSThreshold=2347\nFragThreshold=2346\n"
			"TxBurst=1\nPktAggregate=0\nWmmCapable=1\n"
			"HT_RDG=1\nHT_EXTCHA=0\nHT_OpMode=0\nHT_MpduDensity=4\nHT_BW=0\nHT_BADecline=0\nHT_AutoBA=1\n"
			"HT_AMSDU=0\nHT_BAWinSize=64\nHT_GI=1\nHT_MCS=33\nHT_MIMOPSMode=3\nHT_DisallowTKIP=1\nHT_STBC=0\n",
            ssid);

    if (argc > 4) {
        if (!strcmp(argv[4], "wpa-psk")) {
            asprintf(&fbuf, "%sAuthMode=WPAPSK\nEncrypType=TKIPAES\nWPAPSK=%s\nDefaultKeyID=2\n", wbuf, argv[5]);
        } else if (!strcmp(argv[4], "wpa2-psk")) {
            asprintf(&fbuf, "%sAuthMode=WPA2PSK\nEncrypType=AES\nWPAPSK=%s\nDefaultKeyID=2\n", wbuf, argv[5]);
        } else if (!strcmp(argv[4], "open")) {
            asprintf(&fbuf, "%sAuthMode=OPEN\nEncrypType=NONE\nDefaultKeyID=1\n", wbuf);
        }
    } else {
        asprintf(&fbuf, "%sAuthMode=OPEN\nEncrypType=NONE\nDefaultKeyID=1\n", wbuf);
    }

    fd = open(RALINK_AP_DAT, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    if (fd < 0) {
        ALOGE("Cannot update \"%s\": %s", RALINK_AP_DAT, strerror(errno));
        free(wbuf);
        free(fbuf);
        return -1;
    }

    if (write(fd, fbuf, strlen(fbuf)) < 0) {
        ALOGE("Cannot write to \"%s\": %s", RALINK_AP_DAT, strerror(errno));
        ret = -1;
    }
    close(fd);
    free(wbuf);
    free(fbuf);

    /* Note: apparently open can fail to set permissions correctly at times */
    if (chmod(RALINK_AP_DAT, 0660) < 0) {
        ALOGE("Error changing permissions of %s to 0660: %s",
                RALINK_AP_DAT, strerror(errno));
        unlink(RALINK_AP_DAT);
        return -1;
    }

    if (chown(RALINK_AP_DAT, AID_SYSTEM, AID_WIFI) < 0) {
        ALOGE("Error changing group ownership of %s to %d: %s",
                RALINK_AP_DAT, AID_WIFI, strerror(errno));
        unlink(RALINK_AP_DAT);
        return -1;
    }

	usleep(AP_SET_CFG_DELAY);
#endif
    return ret;
}

/*
 * Arguments:
 *	argv[2] - interface name
 *	argv[3] - AP or P2P or STA
 */
int SoftapController::fwReloadSoftap(int argc, char *argv[])
{
    int i = 0;
    char *fwpath = NULL;

    if (argc < 4) {
        ALOGE("SoftAP fwreload is missing arguments. Please use: softap <wlan iface> <AP|P2P|STA>");
        return ResponseCode::CommandSyntaxError;
    }

    if (strcmp(argv[3], "AP") == 0) {
        fwpath = (char *)wifi_get_fw_path(WIFI_GET_FW_PATH_AP);
    } else if (strcmp(argv[3], "P2P") == 0) {
        fwpath = (char *)wifi_get_fw_path(WIFI_GET_FW_PATH_P2P);
    } else if (strcmp(argv[3], "STA") == 0) {
        fwpath = (char *)wifi_get_fw_path(WIFI_GET_FW_PATH_STA);
    }
    if (!fwpath)
        return ResponseCode::CommandParameterError;
    if (wifi_change_fw_path((const char *)fwpath)) {
        ALOGE("Softap fwReload failed");
        return ResponseCode::OperationFailed;
    }
    else {
        ALOGE("Softap fwReload - Ok");
    }
    return ResponseCode::SoftapStatusResult;
}

void SoftapController::generatePsk(char *ssid, char *passphrase, char *psk_str) {
    unsigned char psk[SHA256_DIGEST_LENGTH];
    int j;
    // Use the PKCS#5 PBKDF2 with 4096 iterations
    PKCS5_PBKDF2_HMAC_SHA1(passphrase, strlen(passphrase),
            reinterpret_cast<const unsigned char *>(ssid), strlen(ssid),
            4096, SHA256_DIGEST_LENGTH, psk);
    for (j=0; j < SHA256_DIGEST_LENGTH; j++) {
        sprintf(&psk_str[j*2], "%02x", psk[j]);
    }
}
